/*
 * Copyright (c) 2023 by frostime (Yp Z), All Rights Reserved.
 * @Author       : Yp Z
 * @Date         : 2023-07-02 14:00:35
 * @FilePath     : /vite.config.js
 * @LastEditTime : 2023-07-02 16:47:04
 * @Description  : Vite Config
 */
import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
    plugins: [sveltekit()],
    build: {
        rollupOptions: {
            external: ["siyuan", "process"]
        }
    }
};

export default config;
