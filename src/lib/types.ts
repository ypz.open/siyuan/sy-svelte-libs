/*
 * Copyright (c) 2023 by frostime (Yp Z), All Rights Reserved.
 * @Author       : Yp Z
 * @Date         : 2023-07-02 00:40:03
 * @FilePath     : /src/lib/types.ts
 * @LastEditTime : 2023-07-08 14:41:35
 * @Description  :
 */

type DocumentId = string;
type BlockId = string;
type NotebookId = string;
type PreviousID = BlockId;
type ParentID = BlockId | DocumentId;

type BlockType = "d" | "s" | "h" | "t" | "i" | "p" | "f" | "audio" | "video" | "other";

type BlockSubType = "d1" | "d2" | "s1" | "s2" | "s3" | "t1" | "t2" | "h1" | "h2" | "h3" | "h4" | "h5" | "h6" | "table" | "task" | "toggle" | "latex" | "quote" | "html" | "code" | "footnote" | "cite" | "collection" | "bookmark" | "attachment" | "comment" | "mindmap" | "spreadsheet" | "calendar" | "image" | "audio" | "video" | "other";

export interface IBlock {
    id: BlockId;
    parent_id?: BlockId;
    root_id: DocumentId;
    hash: string;
    box: string;
    path: string;
    hpath: string;
    name: string;
    alias: string;
    memo: string;
    tag: string;
    content: string;
    fcontent?: string;
    markdown: string;
    length: number;
    type: BlockType;
    subtype: BlockSubType;
    ial?: { [key: string]: string };
    sort: number;
    created: string;
    updated: string;
}


type TSettingItemType = 'checkbox' | 'input' | 'button' | 'select' | 'slider';
type TSettingItemValue = string | number | boolean;
export interface ISettingItem {
    type: TSettingItemType;
    title: string;
    hint: string;
    settingKey: string;
    settingValue: TSettingItemValue;
    placeholder?: string;
    options?: { [key: string]: string };
    slider?: { min: number, max: number, step: number };
}

export interface IBookmarkItem {
    id: BlockId;
    content: string;
}

export interface IBookmarkSection {
    title: string;
    items: IBookmarkItem[];
}

export type {
    DocumentId, BlockId, NotebookId, PreviousID, ParentID, TSettingItemType, TSettingItemValue
}
